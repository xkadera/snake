class Food {
  constructor(boardSize, rowSize) {
    this.x = getRadnomCoord(boardSize);
    this.y = getRadnomCoord(boardSize);
    this.rowSize = rowSize;
  }

  draw() {
    fill(0);
    square(this.x, this.y, this.rowSize);
  }

  getCoords() {
    return [this.x, this.y];
  }
}
