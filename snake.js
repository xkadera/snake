class Snake {
  constructor(canvasSize, rowSize, snakeSize) {
    this.direction = [1, 0];
    this.rowSize = rowSize;
    this.canvasSize = canvasSize;
    this.body = this.createSnake(snakeSize);
  }

  createSnake(snakeSize) {
    var snake = [];
    var middle = Math.floor(this.canvasSize / 2 / 10) * 10;
    for (let i = 0; i < snakeSize; i++) {
      snake.push([middle - this.rowSize * i, middle]);
    }
    return snake;
  }

  draw() {
    fill(255);
    this.body.forEach((element) => {
      square(element[0], element[1], this.rowSize);
    });
  }

  setDir(x, y) {
    this.direction = [x, y];
    print(this.direction);
  }

  getHead() {
    return this.body[0];
  }

  canEat(food) {
    var foodCoords = food.getCoords();
    return this.equals(foodCoords, this.getHead());
  }

  move() {
    this.addInFront();
    this.body.pop();
  }

  isOb() {
    print(this.getHead());
    return (
      this.getHead()[0] < 0 ||
      this.getHead()[0] >= this.canvasSize ||
      this.getHead()[1] < 0 ||
      this.getHead()[1] >= this.canvasSize
    );
  }

  eats() {
    this.addInFront();
  }

  addInFront() {
    var head = this.getHead();
    var newHead = [
      head[0] + this.direction[0] * this.rowSize,
      head[1] + this.direction[1] * this.rowSize,
    ];
    this.body.unshift(newHead);
  }

  hitItself() {
    for (let i = 1; i < this.body.length; i++) {
      if (this.equals(this.body[i], this.getHead())) {
        return true;
      }
    }
    return false;
  }

  equals(first, second) {
    return first[0] == second[0] && first[1] == second[1];
  }
}
