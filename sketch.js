const CANVAS_SIZE = 400;
const ROW_SIZE = 10;
const GAME_SPEED = 5;
const SNAKE_SIZE = 3;

var snake;
var curFood;

function setup() {
  createCanvas(CANVAS_SIZE, CANVAS_SIZE);
  frameRate(GAME_SPEED);
  snake = new Snake(CANVAS_SIZE, ROW_SIZE, SNAKE_SIZE);
  curFood = new Food(CANVAS_SIZE, ROW_SIZE);
}

function draw() {
  background(220);

  if (snake.canEat(curFood)) {
    snake.eats();
    curFood = new Food(CANVAS_SIZE, ROW_SIZE);
  }
  if (snake.isOb() || snake.hitItself()) {
    background(255, 0, 0);
    noLoop();
  }

  snake.draw();
  curFood.draw();
  snake.move();
}

function getRadnomCoord(boardSize) {
  return Math.floor((Math.random() * boardSize) / 10) * 10;
}

function keyPressed() {
  if (keyCode === LEFT_ARROW) {
    snake.setDir(-1, 0);
  } else if (keyCode === RIGHT_ARROW) {
    snake.setDir(1, 0);
  } else if (keyCode === DOWN_ARROW) {
    snake.setDir(0, 1);
  } else if (keyCode === UP_ARROW) {
    snake.setDir(0, -1);
  }
}
